libgd-svg-perl (0.33-3) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Replace libgd-gd2-{,noxpm-}perl with libgd-perl in debian/control.
  * Declare compliance with Debian Policy 4.5.0.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Update short description.

 -- gregor herrmann <gregoa@debian.org>  Wed, 29 Jul 2020 21:57:50 +0200

libgd-svg-perl (0.33-2) unstable; urgency=medium

  * Team upload.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Update to dpkg source format 3.0 (quilt)
  * Declare that the package does not need (fake)root to build
  * Add Testsuite declaration for autopkgtest-pkg-perl
  * Move to minimal dh-style debian/rules

 -- Niko Tyni <ntyni@debian.org>  Sat, 31 Mar 2018 14:30:03 +0300

libgd-svg-perl (0.33-1) unstable; urgency=low

  * New upstream version.
    - Can now embed pixmap information inside an SVG (Lincoln Stein)
    - Improvements to alpha support (Lincoln Stein)
    - Ghost methods for more complete mapping to GD (Jason Stajich)
  * Incremented Standards-Version in debian/control to reflect the
    conformance with Policy 3.8.1 (no changes needed).
  * Updated debian/copyright to a lighter version of the
    machine-readable format.

 -- Charles Plessy <plessy@debian.org>  Sun, 10 May 2009 12:19:33 -0400

libgd-svg-perl (0.32-1) unstable; urgency=low

  * Initial Release (Closes: #517206).

 -- Charles Plessy <plessy@debian.org>  Thu, 26 Feb 2009 20:12:27 +0900
